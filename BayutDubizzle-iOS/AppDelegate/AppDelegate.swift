//
//  AppDelegate.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 05/02/2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Bootstrapper.initialize(launchOptions)
        return true
    }
}
