//
//  StoryBoard.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 05/02/2021.
//

import Foundation

enum StoryBoard: String {
    case main = "Main"
}
