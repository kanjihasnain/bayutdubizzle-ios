//
//  UIImageView.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 06/02/2021.
//

import Foundation
import UIKit
import BayutDubizzleImageCacher

class ImageManager {
    
    static var shared = ImageManager()
    
    static let imageCacher = ImageCacher()
    
    func setImage(with url: String, imageView: UIImageView) {
        ImageManager.imageCacher.downloadAndShowImage(from: url, on: imageView)
    }
}
