//
//  DetailsInteractor.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 07/02/2021.
//

import UIKit

final class DetailsInteractor {

    weak var presenter: DetailsInteractorOutputProtocol?
}

extension DetailsInteractor: DetailsInteractorInputProtocol {

}
