//
//  DetailsTableViewCell.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 07/02/2021.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
