//
//  ItemSliderCollectionViewCell.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 07/02/2021.
//

import UIKit

class ItemSliderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageSlider: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
