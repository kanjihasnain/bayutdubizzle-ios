//
//  SetupViewInteractor.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 05/02/2021.
//

import UIKit

final class SetupViewInteractor {
    
    weak var presenter: SetupViewInteractorOutputProtocol?
}

extension SetupViewInteractor: SetupViewInteractorInputProtocol {
    
    // MARK: Just For Mimicking Ap An API Call Here.
    func getAppMeta() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Bootstrapper.startAppAfterSetup()
        })
    }
}
