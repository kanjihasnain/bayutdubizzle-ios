//
//  SetupViewViewController.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 05/02/2021.
//

import UIKit

final class SetupViewViewController: UIViewController, SetupViewViewProtocol {

	var presenter: SetupViewPresenterProtocol?

	override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}
