//
//  NetworkConstants.swift
//  BayutDubizzle-iOS
//
//  Created by HasnainKanji on 06/02/2021.
//

// MARK: A Separate Struct Can Be Maintained To Keep The Network Constants And URLs
struct NetworkUrls {
    static let itemsUrls = "https://ey3f2y0nre.execute-api.us-east-1.amazonaws.com/default/dynamodb-writer"
}
