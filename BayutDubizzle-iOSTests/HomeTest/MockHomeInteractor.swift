//
//  MockHomeInteractor.swift
//  BayutDubizzle-iOSTests
//
//  Created by HasnainKanji on 07/02/2021.
//

import Foundation
@testable import BayutDubizzle_iOS

// MARK: A Mock Interactor Fetches and Parses Json From Local To Test Our Presenter
final class MockHomeInteractor: HomeInteractorInputProtocol {
    
    weak var presenter: HomeInteractorOutputProtocol?
    
    func getItemsFromServer() {
        if let url = Bundle.main.url(forResource: "items", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([Items].self, from: data)
                presenter?.itemsFetchedSucessfully(jsonData)
            } catch {
                print("error:\(error)")
                fatalError()
            }
        }
    }
}
