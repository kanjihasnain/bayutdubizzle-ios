//
//  MockHomeViewController.swift
//  BayutDubizzle-iOSTests
//
//  Created by HasnainKanji on 07/02/2021.
//

import Foundation
import UIKit
@testable import BayutDubizzle_iOS

final class MockHomeViewController: UIViewController, HomeViewProtocol {
    
    var presenter: HomePresenterProtocol?

    func addEmptyView(_ state: EmptyViewState) {
    }
    
    func showError(with message: String) {
    }
    
    func removeEmptyView() {
    }
    
    func reloadView() {
    }
}
