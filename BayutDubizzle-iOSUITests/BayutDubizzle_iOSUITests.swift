//
//  BayutDubizzle_iOSUITests.swift
//  BayutDubizzle-iOSUITests
//
//  Created by HasnainKanji on 05/02/2021.
//

import XCTest

class BayutDubizzle_iOSUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTableView() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let label = app.staticTexts["Notebook"]
        XCTAssert(label.waitForExistence(timeout: 10), "Failed To Fetch Data From Server In 10 Sec")
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        app.navigationBars["Notebook"].buttons["Back"].tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 5).children(matching: .other).element.swipeUp()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.swipeUp()
        collectionViewsQuery/*@START_MENU_TOKEN@*/.staticTexts["Pineapple, Ananas"]/*[[".cells.staticTexts[\"Pineapple, Ananas\"]",".staticTexts[\"Pineapple, Ananas\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Pineapple, Ananas"].buttons["Back"].tap()
        
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
